function transform(data,to){
    const response = {}
    for(let i in to){
        const dataIndex = to[i]
        if(data[i]){
            response[dataIndex] = data[i]
        }else{
            response[dataIndex] = undefined
        }
    }

    return response
}
function transformAll(data,to){
    const response = []
    for(let rowIndex in data){
        const row = data[rowIndex]
        console.log(row)
        const newRow = {}
        for(let i in to){
            const dataIndex = to[i]
            if(row[i]){
                newRow[dataIndex] = row[i]
            }else{
                newRow[dataIndex] = undefined
            }
        }
        response[rowIndex] = newRow

    }

    return response
}

module.exports = {
    transform,
    transformAll
}