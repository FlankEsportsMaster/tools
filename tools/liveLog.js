const request = require('request')
const colors = require('colors')
colors.setTheme({
    info: 'cyan',
    warn: 'yellow',
    success: 'green',
    error: 'red'
  });

class liveLog {
    server
    app
    source
    time
    token
    io
    isConnected
    constructor({
        server,
        app,
        source,
        token
    }) {

        return (async () => {
            try {
                this.server = server
                this.app = app
                this.source = source
                this.token = token
                this.io = await this.connect()
                this.isConnected = this.io != undefined;
                
                return this
            } catch (e) {
                console.log(e.message)
            }
        })()
    }

    async connect() {

        let io = require('socket.io-client')(this.server, {
            reconnect: true
        })
        
        
        return io

    }
    healthCheck() {
        request.get(`${this.server}/health-check`, (err, res, body) => {
            if (res && res.statusCode == 200) {
                //ok
                return true
        } else {
                throw new Error('socket-is-down')
            }
        })

    }

    success = (log) =>{

        try{
            this.io.emit('log-outer', JSON.stringify({
                app: this.app,
                source: this.source,
                log,
                type: 'success',
                time: Date.now().valueOf()
            }))
            io.on('error',(err)=>{
                this.io = undefined
                throw new Error()
            })
        }
        catch(e){
            const message = this.source ? `${this.source}::${log}`.success.bold : log.success.bold
            console.log(message)
        }
      

    }

    error = (log) => {
        try{
            this.io.emit('log-outer', JSON.stringify({
                app: this.app,
                source: this.source,
                log,
                type: 'error',
                time: Date.now().valueOf()
            }))
            io.on('error',(err)=>{
                this.io = undefined
                throw new Error()
            })
        }
        catch(e){
            const message = this.source ? `${this.source}::${log}`.error.bold : log.error.bold
            console.log(message)
        }
       

    }

    warn = (log) => {
        try{
            this.io.emit('log-outer', JSON.stringify({
                app: this.app,
                source: this.source,
                log,
                type: 'warn',
                time: Date.now().valueOf()
            }))
            io.on('error',(err)=>{
                this.io = undefined
                throw new Error()
            })
        }catch(e){
            const message = this.source ? `${this.source}::${log}`.warn.bold : log.warn.bold
            console.log(message)
        }
       

    }
    trace = (log) => {
        try{
            this.io.emit('log-outer', JSON.stringify({
                app: this.app,
                source: this.source,
                log,
                type: 'trace',
                time: Date.now().valueOf()
            }))
            io.on('error',(err)=>{
                this.io = undefined
                throw new Error()
            })
        }catch(e){
            const message = this.source ? `${this.source}::${log}`.info.bold : log.info.bold
            console.log(message)
        }
       

    }
}
module.exports = liveLog