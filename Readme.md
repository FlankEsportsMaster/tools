## Installation


```bash
npm install git+https://bitbucket.org/FlankEsportsMaster/tools.git
```

## Usage

```javascript
const {
    liveLog,
    dataTypeValidation:{
    isUUID,
    isNumeric,
    isString,
    isEmail,
    stringEqualOrLess,
    stringEqualOrGreater,
    numberMaxValue,
    numberMinValue,
    isObject:isObject,
    assert:{
        isString:isStringAssert,
        isNumeric:isNumericAssert,
        isUUID:isUUIDAssert,
        isEmail: isEmailAssert,
        stringEqualOrLess: stringEqualOrLessAssert,
        stringEqualOrGreater:stringEqualOrGreaterAssert,
        numberMaxValue: numberMaxValueAssert,
        numberMinValue: numberMinValueAssert,
        isObject:isObjectAssert

    }
}
} = require('@flankcommon/tools')

const log = await new liveLog({
        server: 'http://localhost:3333',
        app: 'testApp',
        source:'application',
        token:'testkey'
    })
log.success('connected successfully')
log.error('error')
log.warn('warning')
```

