const {
    liveLog,
    dataTypeValidation,
    Str,
    bumblebee
    
} = require('./tools')

async function t(){
    const log = await new liveLog({
        server: 'http://localhost:3333',
        app: 'testApp',
        source:'application',
        token:'testkey'
    })
    log.success('connected')
}
function test(){
    const {transform, transformAll} = bumblebee
    // console.log(Str.removeSpecial('2hello12||dsa^!^+^%+&%/&(/&('))
    const newid = Str.newId()
    // console.log(newid)


    console.log(transformAll([{a: 'a', b: 'b'},{a: 'a', b: 'b'}],{a:'firstParam',b:'secondParam'}))
}

module.exports = {
    liveLog,
    dataTypeValidation,
    Str,
    bumblebee
}